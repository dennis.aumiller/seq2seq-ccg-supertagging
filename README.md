# Sequence-to-sequence CCG supertagging

This is the accompanying code for our project, "Supertagging with a Sequence2Sequence NN". It is written
in Python using [TensorFlow](https://www.tensorflow.org/).


## Installation

This code requires Python versions 3.4+. We recommend to setup a virtual environment. In this environment, you can
install the required libraries with `pip install -r requirements.txt`. This will also install TensorFlow, but from
the generic package published on PyPI. Since we do some fairly costly computations, it is recommended to build
TensorFlow on the target machine from source to take advantage of machine-specific optimizations and/or GPU support.
Cf. the [TensorFlow installation docs](https://www.tensorflow.org/install/) for more information.


## Using the Seq2Seq Supertagger

The Seq2Seq Supertagger is implemented as a command-line program using [click](http://click.pocoo.org/6/). It provides
the following commands:

```
$ python main.py --help
Usage: main.py [OPTIONS] COMMAND [ARGS]...

Options:
  --help  Show this message and exit.

Commands:
  analyze   Analyze stats file & logs
  ccgbank   Extract and convert data from CCGBank
  evaluate  Evaluate a trained model
  train     Train the seq2seq supertagger model
```


### Training the model

To train the model, we first need the training data, which can be downloaded [here](http://appositive.cs.washington.edu/resources/).
This is the same data as used by Lewis et al. (2016). Assuming you've downloaded the training data to a directory called `data`,
you can start the training like this:

```
$ python main.py train --dev-data=./data/dev.stagged --training-data=./data/train.stagged --categories=./data/categories --embeddings=./data/embeddings.raw
```

These are the only required options, all others have defaults defined.
During training, model checkpoints and a log file will be saved to the directory `output/`. You can configure
this output directory with the `--output-path` option. It's also often helpful to distinguish between different experiments.
By default the experiment will be called `seq2seq`, so you will find the output of the above command in the directory
`output/seq2seq/`. To change the experiment name, use the `-e`/`--experiment` option.

Furthermore, you can adjust almost any model parameter we use via various options. Here is a complete list of the available ones:

```
$ python main.py train --help
Usage: main.py train [OPTIONS]

  Train the seq2seq supertagger model

Options:
  --categories PATH               Path to the categories file
  --embeddings PATH               Path to the embeddings file
  --dev-data PATH                 Path to the development data
  --training-data PATH            Path to the training data
  --tritrain-data PATH            Path to the tritrain data
  -o, --output-path PATH          Output directory
  -e, --experiment TEXT           Experiment name (use to distinguish between
                                  different runs)
  --batch-size INTEGER            Size of one batch used for training
  --layers INTEGER                Number of layers in the model
  --units INTEGER                 Number of hidden units
  --learning-rate FLOAT           Learning rate to use for training
  --learning-rate-decay-factor FLOAT
                                  How much to decrease learning rate
  --max-gradient-norm FLOAT       Maximum gradient norm after which gradients
                                  will be clipped
  --bucket-size INTEGER           Size of the buckets for bucketized training
  --max-tokens INTEGER            Maximum number of tokens in a sentence
                                  (larger ones will be ignored)
  --cell-type [gru|lstm]          RNN cell type to use
  --steps-per-checkpoint INTEGER  How many training steps to do per checkpoint
  --help                          Show this message and exit.
```


### Evaluating the trained model

To evaluate the trained model, you can use the `evaluate` subcommand. It supports almost the same options as the `train`
subcommand described above. To evaluate all the checkpoints in an experiment, run

```
$ python main.py evaluate --eval-data=./data/dev.stagged --categories=./data/categories --embeddings=./data/embeddings.raw --all
```

This will read the model parameters from all checkpoints that can be found in the default output directory (`output/seq2seq/`)
and evaluate the model against the data given by the `--eval-data` option. Again, you can adjust the output directory and
the experiment name with the `--output-path` and `-e`/`--experiment` options respectively. To run the model with a specific
checkpoint only, you can use the `--checkpoint-number` option. Finally, if you omit the `--all` option, the evaluation
will use the last checkpoint available.

The evaluation will calculate the overall tagging accuracy for every checkpoint as well as the accuracy by bucket and
write the results to a JSON file in the output directory which is called `stats.json` by default. You can use this
file as an input to the `analyze` subcommand, as described below.

Here is the complete usage information:

```
$ python main.py evaluate --help
Usage: main.py evaluate [OPTIONS]

  Evaluate a trained model

Options:
  --categories PATH               Path to the categories file
  --embeddings PATH               Path to the embeddings file
  --eval-data PATH                Path to the data to evaluate the model with
  -o, --output-path PATH          Output directory
  -e, --experiment TEXT           Experiment name
  -s, --stats-file PATH           Name of the stats output file
  -c, --checkpoint-number INTEGER
                                  Number of the checkpoint to use
  --all                           Evaluate all checkpoints
  --bucket-size INTEGER           Size of the buckets for bucketized training
  --layers INTEGER                Number of layers in the model
  --units INTEGER                 Number of hidden units
  --learning-rate FLOAT           Learning rate to use for training
  --learning-rate-decay-factor FLOAT
                                  How much to decrease learning rate
  --max-gradient-norm FLOAT       Maximum gradient norm after which gradients
                                  will be clipped
  --max-tokens INTEGER            Maximum number of tokens in a sentence
                                  (larger ones will be ignored)
  --cell-type [gru|lstm]          RNN cell type to use
  --help                          Show this message and exit.
```


### Analyzing the results

After evaluation, it's time to analyze the results. This can be achieved by running

```
$ python main.py analyze
```

Again, adjust the `--output-path` and `-e`/`--experiment` options as needed. The required input files for this command
are the stats file from the evaluation (can be set via the `-s`/`--stats-file` option) and the logfile from the training
run, usually simply called `log` in the output directory (can be set via the `-l`, `--logfile` option).

This command will produce various plots to help with the result analysis.

Here is the complete usage information:

```
$ python main.py analyze --help
Usage: main.py analyze [OPTIONS]

  Analyze stats file & logs

Options:
  -o, --output-path PATH  Output directory
  -e, --experiment TEXT   Experiment name
  -s, --stats-file PATH   Name of the stats output file
  -l, --logfile PATH      Name of the log file
  --help                  Show this message and exit.
```
