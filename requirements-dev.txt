-r requirements.txt

# Coding style
flake8==3.3.0

# Testing
pytest==3.0.7