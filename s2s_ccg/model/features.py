import collections

from s2s_ccg.markers import Markers


class AbstractFeatureSpace():
    def __init__(self, filename):
        self.filename = filename
        self.raw_data = []
        self.space = [
            Markers.PAD,
            Markers.START,
            Markers.END,
            Markers.UNKNOWN,
            Markers.OUT_OF_RANGE,
        ]
        self.index_space = None

        self.read_file()

    def read_file(self):
        with open(self.filename, 'r') as f:
            self.raw_data = f.readlines()

    def index(self, feature):
        """Get index of given feature."""
        return self.index_space[feature]

    def feature(self, index):
        """Get feature by given index."""
        return self.space[index]

    def size(self):
        """Return size of feature space."""
        return len(self.space)


class SupertagSpace(AbstractFeatureSpace):
    def __init__(self, filename):
        super().__init__(filename)
        self.create_space()
        self.create_index_space()

    def create_space(self):
        self.space.extend(sample.strip() for sample in self.raw_data)

    def create_index_space(self):
        self.index_space = collections.defaultdict(
            lambda: 0,
            {feature: index for index, feature in enumerate(self.space)}
        )
