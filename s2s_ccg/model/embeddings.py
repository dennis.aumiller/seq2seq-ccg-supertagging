import collections

from s2s_ccg.markers import Markers
from s2s_ccg.model.features import AbstractFeatureSpace


class AbstractEmbeddingSpace(AbstractFeatureSpace):
    def __init__(self, filename):
        super().__init__(filename)

        self.embeddings = []
        self.embedding_size = None

    def extract(self, token):
        raise NotImplementedError


class TurianEmbeddingSpace(AbstractEmbeddingSpace):
    def __init__(self, filename):
        super().__init__(filename)

        first_sample = self.raw_data[0].split()
        first_word = first_sample[0]

        # Check if the unknown word is the first embedding
        if first_word != Markers.UNKNOWN:
            raise ValueError(
                'First embedding in the file should represent '
                'the unknown word.'
            )

        # Initialize embedding size
        self.embedding_size = len(first_sample[1:])

        # Add embeddings for special symbols
        for el in self.space:
            if el != Markers.UNKNOWN:
                self.embeddings.append(tuple([0.0] * self.embedding_size))
            else:
                self.embeddings.append(tuple(float(n) for n in first_sample[1:]))

        self.create_space()
        self.create_index_space()

    def create_space(self):
        for sample in self.raw_data:
            word = sample.split()[0].lower()
            self.space.append(word)

    def create_space_and_embeddings(self):
        added_words = set()

        for idx, sample in enumerate(self.raw_data):
            split_sample = sample.split()

            word = split_sample[0].lower()

            if word in added_words:
                continue

            embedding = tuple(float(n) for n in split_sample[1:])

            # Check embedding size, raise error if it doesn't match
            if len(embedding) != self.embedding_size:
                raise ValueError(
                    'Dimensions mismatch. Expected {} but was {}.'.format(
                        self.embedding_size,
                        len(embedding),
                    )
                )

            self.space += (word,)
            self.embeddings += (embedding,)

        self._add_extra_tokens()

    def create_index_space(self):
        self.index_space = collections.defaultdict(
            lambda: Markers.UNKNOWN_ID,
            {feature: index for index, feature in enumerate(self.space)},
        )

    def extract(self, token):
        return token.lower()


class WordSpace(AbstractEmbeddingSpace):
    def extract(self, token):
        return token.lower()
