import collections
import logging

import numpy as np

from s2s_ccg.ccgbank import StaggedReader
from s2s_ccg.defaults import DEFAULT_BUCKET_SIZE, DEFAULT_MAX_TOKENS
from s2s_ccg.markers import Markers
from s2s_ccg.utils import tempinput


class Data():
    def __init__(self,
                 filename,
                 supertag_space,
                 embedding_space,
                 bucket_size=DEFAULT_BUCKET_SIZE,
                 max_tokens=DEFAULT_MAX_TOKENS):
        self.filename = filename
        self.supertag_space = supertag_space
        self.embedding_space = embedding_space
        self.bucket_size = bucket_size
        self.max_tokens = max_tokens

        self.reader = StaggedReader(filename)
        self.buckets = self._get_buckets()

        self.bucketized_sentences = collections.defaultdict(list)

        self._load_data()
        self.bucketize()

    def __add__(self, other):
        all_sentences = self.reader.raw_sentences + other.reader.raw_sentences

        with tempinput('\n'.join(all_sentences)) as tmp:
            self.reader = StaggedReader(tmp)
            self._load_data()

        self.buckets = self._get_buckets()
        self.bucketized_sentences = collections.defaultdict(list)
        self.bucketize()

        return self

    @property
    def size(self):
        return len(self.reader.raw_sentences)

    def _get_buckets(self):
        return tuple(range(
            self.bucket_size,
            self.max_tokens + self.bucket_size,
            self.bucket_size
        ))

    def _load_data(self):
        self.reader.get_sentences()

    def get_bucket(self, sentence_len):
        """Get bucket for given sentence length.

        Returns:
            int: The bucket the sentence belongs to, identified by the number
                of maximum words allowed in the sentence. I.e., also re
        """
        if sentence_len > self.max_tokens:
            # Use a separate bucket for sentences which are too long
            return -1

        return self.buckets[sentence_len // self.bucket_size]

    def bucket_id(self, bucket):
        return self.buckets.index(bucket)

    def bucketize(self):
        """Map sentences from the stagged file to their respective bucket."""
        if not self.reader.raw_sentences:
            self._load_data()

        # Sort sentences by length first
        self.reader.raw_sentences = sorted(
            self.reader.raw_sentences,
            key=len,
        )

        # Align the sorted sentences
        self.reader.align_sentences()

        # Map the sorted & aligned sentences to their bucket
        for sentence in self.reader.aligned_sentences:
            bucket = self.get_bucket(len(sentence[0]))
            self.bucketized_sentences[bucket].append(sentence)

    def tensorize(self, sentence, bucket):
        """Create a tensor representation of an aligned sentence."""
        words, supertags, _ = sentence

        if len(words) != len(supertags):
            raise ValueError(
                'Number of words ({}) should match '
                'number of supertags ({}).'.format(
                    len(words),
                    len(supertags),
                )
            )

        encoder_input = np.array(
            [self.embedding_space.index(w.lower()) for w in words if w]
        )
        decoder_input = np.array(
            [self.supertag_space.index(s) for s in supertags] + [Markers.END_ID]
        )

        return encoder_input, decoder_input


def read_data(supertag_space, turian_space, data_path, bucket_size, max_tokens):
    logging.info('Loading data from \'{}\''.format(data_path))
    data = Data(data_path, supertag_space, turian_space, bucket_size=bucket_size, max_tokens=max_tokens)
    logging.info('Loaded {} sentences from \'{}\''.format(data.size, data_path))

    return data
