import datetime
import logging
import os

import numpy as np

from s2s_ccg.utils import perplexity


def bucket_distribution(data):
    bucket_sizes = [len(data.bucketized_sentences[b]) for b in data.buckets]
    distribution = [
        sum(bucket_sizes[:i+1]) / data.size for i in range(len(bucket_sizes))
    ]

    return distribution


def get_random_bucket(buckets, distribution):
    # Choose a random number between 0 and 1
    random_number = np.random.random_sample()

    possible_buckets = [
        i for i in range(len(distribution)) if distribution[i] > random_number
    ]

    # Sometimes, the random number is bigger than anything in the distribution
    # Then we just choose the last bucket, because this one is usually the rarest one
    # and thereby underrepresented during training
    if possible_buckets:
        return buckets[min(possible_buckets)]
    else:
        return buckets[-1]


def maybe_evaluate(model,
                   session,
                   dev_data,
                   current_step,
                   steps_per_checkpoint,
                   step_time,
                   loss,
                   previous_losses,
                   output_path,
                   experiment):
    if (current_step == 0) or (current_step % steps_per_checkpoint != 0):
        return True, False

    logging.info('=' * 80)
    logging.info('Evaluating at step {}'.format(current_step))
    logging.info('=' * 80)

    # Calculate perplexity
    step_perplexity = perplexity(loss)

    # Log info on some key metrics
    logging.info('Global step: {}'.format(model.global_step.eval()))
    logging.info('Learning rate: {}'.format(model.learning_rate.eval()))
    logging.info('Time taken for step: {}'.format(datetime.timedelta(seconds=step_time)))
    logging.info('Perplexity: {}'.format(step_perplexity))

    # Decrease learning rate if no improvement was seen the last 3 times
    #if len(previous_losses) > 2 and loss > max(previous_losses[-3:]):
    #    session.run(model.learning_rate_decay_op)

    # Update previous losses
    previous_losses.append(loss)

    # Save checkpoint
    checkpoint_path = os.path.join(output_path, experiment)

    if not os.path.exists(checkpoint_path):
        logging.info('Creating checkpoint path at {}'.format(checkpoint_path))
        os.makedirs(checkpoint_path)

    checkpoint_file_path = os.path.join(checkpoint_path, '{}.ckpt'.format(experiment))

    logging.info('Saving checkpoint file at {}'.format(checkpoint_file_path))
    model.saver.save(session, checkpoint_file_path, global_step=model.global_step)

    # Calculate perplexity by bucket with development set
    logging.info('-' * 80)
    logging.info('Calculating perplexity per dev data bucket')
    logging.info('-' * 80)

    for bucket in dev_data.buckets:
        if len(dev_data.bucketized_sentences[bucket]) == 0:
            logging.info('Skipping empty bucket {}'.format(bucket))
            continue

        encoder_inputs, decoder_inputs, weights = model.get_batch(
            dev_data,
            bucket,
        )
        _, bucket_loss, output_logits = model.step(
            session,
            encoder_inputs,
            decoder_inputs,
            weights,
            bucket,
            dev_data.bucket_id(bucket),
            True,
        )

        # outputs = np.apply_along_axis(np.argmax, 2, output_logits)
        # now compare with decoder_inputs
        bucket_perplexity = perplexity(bucket_loss)
        logging.info('Evaluated bucket {}: Perplexity {}'.format(
            bucket,
            bucket_perplexity,
        ))

    logging.info('-' * 80)

    return True, True
