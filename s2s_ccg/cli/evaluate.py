import logging
import json
import os

import click
import tensorflow as tf

from s2s_ccg.data import read_data
from s2s_ccg.defaults import (
    DEFAULT_BUCKET_SIZE,
    DEFAULT_LAYERS,
    DEFAULT_LEARNING_RATE,
    DEFAULT_LEARNING_RATE_DECAY_FACTOR,
    DEFAULT_MAX_GRADIENT_NORM,
    DEFAULT_MAX_TOKENS,
    DEFAULT_UNITS,
    RNN_CELL_CHOICES,
)
from s2s_ccg.evaluate import evaluate_checkpoint
from s2s_ccg.model import create_model, init_spaces
from s2s_ccg.utils import update_checkpoint_file, get_checkpoint_path


@click.command(help='Evaluate a trained model')
@click.option('--categories', type=click.Path(), help='Path to the categories file')
@click.option('--embeddings', type=click.Path(), help='Path to the embeddings file')
@click.option('--eval-data', type=click.Path(), help='Path to the data to evaluate the model with')
@click.option('--output-path', '-o', type=click.Path(), default='output', help='Output directory')
@click.option('--experiment', '-e', type=click.STRING, default='seq2seq', help='Experiment name')
@click.option('--stats-file', '-s', type=click.Path(), default='stats.json', help='Name of the stats output file')
@click.option('--checkpoint-number', '-c', type=click.INT, help='Number of the checkpoint to use')
@click.option('--all', is_flag=True, help='Evaluate all checkpoints')
@click.option('--bucket-size', type=click.INT, default=DEFAULT_BUCKET_SIZE, help='Size of the buckets for bucketized training')
@click.option('--layers', type=click.INT, default=DEFAULT_LAYERS, help='Number of layers in the model')
@click.option('--units', type=click.INT, default=DEFAULT_UNITS, help='Number of hidden units')
@click.option('--learning-rate', type=click.FLOAT, default=DEFAULT_LEARNING_RATE, help='Learning rate to use for training')
@click.option('--learning-rate-decay-factor', type=click.FLOAT, default=DEFAULT_LEARNING_RATE_DECAY_FACTOR, help='How much to decrease learning rate')
@click.option('--max-gradient-norm', type=click.FLOAT, default=DEFAULT_MAX_GRADIENT_NORM, help='Maximum gradient norm after which gradients will be clipped')
@click.option('--max-tokens', type=click.INT, default=DEFAULT_MAX_TOKENS, help='Maximum number of tokens in a sentence (larger ones will be ignored)')
@click.option('--cell-type', type=click.Choice(RNN_CELL_CHOICES), default='gru', help='RNN cell type to use')
def evaluate(categories,
             embeddings,
             eval_data,
             output_path,
             experiment,
             stats_file,
             checkpoint_number,
             all,
             bucket_size,
             layers,
             units,
             learning_rate,
             learning_rate_decay_factor,
             max_gradient_norm,
             max_tokens,
             cell_type):
    # Sanity checks
    checkpoint_path = os.path.abspath(os.path.join(output_path, experiment))
    update_checkpoint_file(checkpoint_path)
    ckpt = tf.train.get_checkpoint_state(checkpoint_path)

    if checkpoint_number:
        eval_checkpoints = [get_checkpoint_path(checkpoint_path, experiment, checkpoint_number)]
    elif all:
        eval_checkpoints = list(ckpt.all_model_checkpoint_paths)
    else:
        # Take the last checkpoint by default
        eval_checkpoints = [ckpt.model_checkpoint_path]

    # Initialize spaces & read data
    supertag_space, turian_space = init_spaces(
        os.path.abspath(categories),
        os.path.abspath(embeddings),
    )
    data = read_data(
        supertag_space,
        turian_space,
        eval_data,
        bucket_size,
        max_tokens
    )

    with tf.Session() as session:
        # Create the model, read parameters from checkpoint if it exists
        logging.info('Creating model')
        model = create_model(
            session,
            turian_space.size(),
            supertag_space.size(),
            data.buckets,
            1,
            units,
            layers,
            learning_rate,
            learning_rate_decay_factor,
            max_gradient_norm,
            cell_type,
            forward_only=True,
        )

        all_stats = {}

        for ckpt_path in eval_checkpoints:
            if tf.train.checkpoint_exists(ckpt_path):
                logging.info('Reading model parameters from \'{}\''.format(ckpt_path))
                model.saver.restore(session, ckpt_path)
            else:
                logging.error('Checkpoint does not exist, aborting.')
                return

            stats = evaluate_checkpoint(model, session, ckpt_path, data, supertag_space, turian_space)

            all_stats[ckpt_path] = stats

        stats_filepath = os.path.join(checkpoint_path, stats_file)

        with open(stats_filepath, 'wt') as f:
            logging.info('Writing stats to {}'.format(stats_filepath))
            f.write(json.dumps(all_stats, sort_keys=True, indent=2))
