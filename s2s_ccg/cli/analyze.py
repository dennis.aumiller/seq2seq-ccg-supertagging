import json
import logging
import os

import click

from s2s_ccg.analysis import analyze_log, create_plots, find_best_checkpoint


@click.command(help='Analyze stats file & logs')
@click.option('--output-path', '-o', type=click.Path(), default='output', help='Output directory')
@click.option('--experiment', '-e', type=click.STRING, default='seq2seq', help='Experiment name')
@click.option('--stats-file', '-s', type=click.Path(), default='stats.json', help='Name of the stats file')
@click.option('--logfile', '-l', type=click.Path(), default='log', help='Name of the log file')
@click.option('--analysis-file', '-a', type=click.Path(), default='analysis.json', help='Name of the analysis file to write')
def analyze(output_path, experiment, stats_file, logfile, analysis_file):
    stats_filepath = os.path.abspath(os.path.join(output_path, experiment, stats_file))
    log_filepath = os.path.abspath(os.path.join(output_path, experiment, logfile))
    analysis_filepath = os.path.abspath(os.path.join(output_path, experiment, analysis_file))

    if not os.path.exists(stats_filepath):
        logging.error('Couldn\'t find stats file at {}'.format(stats_filepath))
        return

    if not os.path.exists(log_filepath):
        logging.error('Couldn\'t find logfile at {}'.format(log_filepath))
        return

    logging.info('Analyzing logfile...')

    with open(log_filepath, 'r') as f:
        log_data = f.read()

    train_stats = analyze_log(log_data)

    with open(stats_filepath, 'r') as json_file:
        json_data = json.load(json_file)

    best_ckpt = find_best_checkpoint(json_data)

    logging.info('Best checkpoint was \'{}\', with the following stats:'.format(best_ckpt))
    logging.info('{}'.format(json.dumps(json_data[best_ckpt], indent=2, sort_keys=True)))

    logging.info('Writing analysis results to \'{}\''.format(analysis_file))

    analysis = {}
    analysis['best'] = {}
    analysis['best']['ckpt'] = best_ckpt
    analysis['best']['stats'] = json_data[best_ckpt]
    analysis['all'] = train_stats

    with open(analysis_filepath, 'wt') as f:
        f.write(json.dumps(analysis, indent=2, sort_keys=True))

    logging.info('Creating plots from stats file...')
    create_plots(json_data, os.path.abspath(os.path.join(output_path, experiment)))
    logging.info('Plots done.')
