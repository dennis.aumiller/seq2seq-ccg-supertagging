import glob
import logging
import math
import os
import re
import tempfile
from contextlib import contextmanager

import tensorflow as tf


def perplexity(loss):
    return math.exp(float(loss)) if loss < 300 else float('inf')


def numeric_sorted(iterable):
    return sorted(
        iterable,
        key=lambda var: [int(x) if x.isdigit() else x for x in re.findall(r'[^0-9]|[0-9]+', var)]
    )


def update_checkpoint_file(checkpoint_path):
    abs_checkpoint_path = os.path.abspath(checkpoint_path)
    ckpt = tf.train.get_checkpoint_state(abs_checkpoint_path)

    ckpt_files = list(numeric_sorted(
        [f.strip('.index') for f in glob.glob('{}/*.index'.format(abs_checkpoint_path))]
    ))

    #if len(ckpt.all_model_checkpoint_paths) == len(ckpt_files):
    #    return

    tf.train.update_checkpoint_state(checkpoint_path, ckpt_files[-1], ckpt_files[:-1])


def get_checkpoint_path(checkpoint_path, prefix, checkpoint_number):
    ckpt = tf.train.get_checkpoint_state(checkpoint_path)
    ckpt_filepath = os.path.abspath(os.path.join(checkpoint_path, prefix + '.ckpt-' + str(checkpoint_number)))

    if ckpt_filepath not in ckpt.all_model_checkpoint_paths:
        logging.error('Checkpoint {} does not exist, aborting.'.format(ckpt_filepath))
        return

    ckpt_idx = list(ckpt.all_model_checkpoint_paths).index(ckpt_filepath)

    return ckpt.all_model_checkpoint_paths[ckpt_idx]


@contextmanager
def tempinput(data):
    temp = tempfile.NamedTemporaryFile(mode='wt', delete=False)
    temp.write(data)
    temp.close()

    try:
        yield temp.name
    finally:
        os.unlink(temp.name)
