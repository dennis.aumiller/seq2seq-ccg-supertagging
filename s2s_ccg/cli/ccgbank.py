import click
from s2s_ccg.ccgbank.ccgbank_reader import CCGBankReader


@click.command(help='Extract and convert data from CCGBank')
@click.option('--ccgbank', type=click.Path(), help='Path to the CCGbank main directory')
@click.option('--auto-path', type=click.Path(), help='Additional path to the AUTO files within the CCGbank. Default is data/AUTO/ in the reader')
@click.option('--parse-ccgbank', type=click.BOOL, default=False, help='Enable to parse the CCGbank files. May take several minutes.')
@click.option('--extract-ccg', type=click.BOOL, default=False, help='Enable to extract the ccg tags from the parsed structure.')
def ccgbank(ccgbank,
            auto_path,
            parse_ccgbank,
            extract_ccg):
	ccgreader = CCGBankReader(ccgbank)

	if parse_ccgbank or extract_ccg:
		if auto_path:
			ccgreader.data_parser(auto_path)
		else:
			ccgreader.data_parser()

	if extract_ccg:
		ccgreader.ccg_extractor()
