class Markers():
    PAD = '*PAD*'
    PAD_ID = 0

    START = '*GO*'
    START_ID = 1

    END = '*EOS*'
    END_ID = 2

    UNKNOWN = '*UNKNOWN*'
    UNKNOWN_ID = 3

    OUT_OF_RANGE = '*OOR*'
    OUT_OF_RANGE_ID = 4
