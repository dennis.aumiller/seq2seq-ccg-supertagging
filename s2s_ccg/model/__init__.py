import logging

from .features import SupertagSpace
from .embeddings import TurianEmbeddingSpace
from .seq2seq import Seq2SeqSupertaggerModel


def create_model(session,
                 source_vocab_size,
                 target_vocab_size,
                 buckets,
                 batch_size,
                 units,
                 layers,
                 learning_rate,
                 learning_rate_decay_factor,
                 max_gradient_norm,
                 cell_type,
                 forward_only=False):
    return Seq2SeqSupertaggerModel(
        source_vocab_size,
        target_vocab_size,
        buckets,
        batch_size,
        units,
        layers,
        learning_rate,
        learning_rate_decay_factor=learning_rate_decay_factor,
        max_gradient_norm=max_gradient_norm,
        cell_type=cell_type,
        forward_only=forward_only,
    )


def init_spaces(categories, embeddings):
    logging.info('Loading supertags from \'{}\''.format(categories))
    supertag_space = SupertagSpace(categories)
    logging.info('Loaded {} supertags'.format(supertag_space.size()))

    logging.info('Loading embeddings from \'{}\''.format(embeddings))
    turian_space = TurianEmbeddingSpace(embeddings)
    logging.info('Loaded {} embeddings'.format(turian_space.size()))

    return supertag_space, turian_space
