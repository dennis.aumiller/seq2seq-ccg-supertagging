import json
import os
import random
import re
import statistics

import matplotlib.pyplot as plt


LOG_EVAL_RE = re.compile(r'^[=]{80}\n(.*?\n)*?Evaluated bucket 105:.*?\n[-]{80}', re.MULTILINE)
STEP_RE = re.compile(r'^Step (\d+).*?\nUsing\sbucket\s(\d+)', re.MULTILINE)
BUCKET_PERPLEXITY = re.compile(r'^Evaluated bucket (\d+): Perplexity (\d+\.\d+)$')


def analyze_log(log_data, step_size=50):
    training_stats = {}
    evals = [match.group(0) for match in re.finditer(LOG_EVAL_RE, log_data)]
    steps = [match.groups() for match in re.finditer(STEP_RE, log_data)]

    for idx, eval in enumerate(evals):
        lines = eval.split('\n')

        global_step = int(lines[3].split(':')[1])
        learning_rate = float(lines[4].split(':')[1])
        perplexity = float(lines[6].split(':')[1])
        chosen_buckets = [
            int(m[1]) for m in steps[idx * step_size:idx * step_size + step_size]
        ]
        bucket_data = lines[11:]
        bucket_perplexities = {}

        for bucket_info in bucket_data:
            match = re.match(BUCKET_PERPLEXITY, bucket_info)

            if not match:
                continue

            bucket = int(match.group(1))
            bucket_perplexity = float(match.group(2))

            bucket_perplexities[bucket] = bucket_perplexity

        training_stats[global_step] = {}
        training_stats[global_step]['learning_rate'] = learning_rate
        training_stats[global_step]['perplexity'] = perplexity
        training_stats[global_step]['bucket_perplexities'] = bucket_perplexities
        training_stats[global_step]['chosen_buckets'] = chosen_buckets
        training_stats[global_step]['median_bucket'] = statistics.median(chosen_buckets)
        training_stats[global_step]['mean_bucket'] = statistics.mean(chosen_buckets)

    return training_stats


def find_best_checkpoint(json_data):
    return max(json_data.keys(), key=(lambda key: json_data[key]['tag_accuracy']))


def create_plots(json_data, output_path):
    # for overall accuracy evolution plot
    total_acc = []
    total_idx = []
    max_idx = 0
    max_acc = 0

    all_vals = []
    # extract top level keys
    i = 0
    for key in json_data.keys():

        # do same thing for each bucket

        # for later sorting, store the lengths
        sorter = []
        # but also values
        vals = []
        for length in json_data[key]['bucket_stats'].keys():
            sorter.append(int(length))
            vals.append(json_data[key]['bucket_stats'][length]['tag_accuracy'])

        # also store the total_accuracy over each iteration.
        # find the checkpoint number by searching the last 10 digits.
        sep = key[-10:].find('-')
        curr_idx = int(key[-10 + sep + 1:])
        total_idx.append(curr_idx)
        curr_acc = json_data[key]['tag_accuracy']
        total_acc.append(curr_acc)

        if (curr_acc > max_acc):
            max_acc = curr_acc
            max_idx = i

        # do this for each image
        # Schwartzian transformation
        vals.sort(key=dict(zip(vals, sorter)).get)
        all_vals.append(vals)

        # get sorted values
        sorter.sort()

        # plot the accuracy versus length
        axes = plt.gca()
        axes.set_xlim([0, 115])
        axes.set_ylim([0, 1])
        axes.set_xlabel('Sentence Length in Words')
        axes.set_ylabel('Accuracy')
        axes.set_title('Sentence Length Accuracy, Iteration ' + str(curr_idx).zfill(5))
        fig1 = plt.scatter(sorter, vals)
        fig1 = plt.plot(sorter, vals)
        axes.plot()
        filename = os.path.join(output_path, "test_length_accuracy_" + str(curr_idx).zfill(5) + ".png")
        plt.savefig(filename)
        plt.close()

        i += 1

    # generate iteration numbers from that by sorting
    total_acc.sort(key=dict(zip(total_acc, total_idx)).get)
    max_vals = all_vals[max_idx]
    all_sorted_vals = sorted(list(zip(all_vals, total_idx)), key=lambda x: x[-1])

    total_idx.sort()

    axes2 = plt.gca()
    axes2.set_xlim([0, max(total_idx) + 50])
    axes2.set_ylim([0, 1])
    axes2.set_xlabel('Iteration')
    axes2.set_ylabel('Accuracy')
    axes2.set_title('Accuracy Evolution over Iterations')
    # fig2 = plt.scatter(iterations, total_acc)
    fig2 = plt.plot(total_idx, total_acc)
    axes2.plot()
    plt.savefig(os.path.abspath(os.path.join(output_path, 'accuracy_evolution.png')))
    plt.close()

    # multiplot from randomly sampled generations + best generation

    bad = random.choice(all_sorted_vals[0:50])
    length = len(all_sorted_vals)
    middle = random.choice(all_sorted_vals[int(length / 2):int(length - length / 8)])

    axes3 = plt.gca()
    axes3.set_xlim([0, 115])
    axes3.set_ylim([0, 1])
    axes3.set_xlabel('Sentence Length in Words')
    axes3.set_ylabel('Accuracy')
    fig3 = plt.scatter(sorter, bad[0], color='blue')
    fig3 = plt.plot(sorter, bad[0], label='early stage')
    fig3 = plt.scatter(sorter, middle[0], color='green')
    fig3 = plt.plot(sorter, middle[0], label='later stages')
    fig3 = plt.scatter(sorter, max_vals, color='red')
    fig3 = plt.plot(sorter, max_vals, label='best stage')
    fig3 = plt.legend()
    plt.savefig(os.path.abspath(os.path.join(output_path, 'at_a_glance.png')))
    plt.close()



def perplexity_plots(json_data, output_path):

    # assume sorted perplexities as it was in log_stats_full
    iteration = []
    avg_length = []
    mean_length = []
    overall_perp = []

    for key in json_data.keys():
        curr_idx = int(key)

        curr_vals = []
        curr_it = []
        for perps in json_data[key]["bucket_perplexities"].keys():
            curr_it.append(int(perps))
            curr_vals.append(json_data[key]["bucket_perplexities"][perps])

        curr_vals.sort(key=dict(zip(curr_vals, curr_it)).get)
        curr_it.sort()

        axes = plt.gca()
        axes.set_xlim([0, 115])
        axes.set_ylim([0, 30])
        axes.set_xlabel('Sentence Length in Words')
        axes.set_ylabel('Perplexity')
        axes.set_title('Sentence Length Perplexity, Iteration ' + str(curr_idx).zfill(5))
        fig1 = plt.scatter(curr_it, curr_vals)
        fig1 = plt.plot(curr_it, curr_vals)
        axes.plot()
        filename = os.path.join(output_path, "length_perplexity_" + str(curr_idx).zfill(5) + ".png")
        plt.savefig(filename)
        plt.close()

        iteration.append(curr_idx)
        avg_length.append(json_data[key]["mean_bucket"])
        mean_length.append(json_data[key]["median_bucket"])
        overall_perp.append(json_data[key]["perplexity"])

    avg_length.sort(key=dict(zip(avg_length, iteration)).get)
    mean_length.sort(key=dict(zip(mean_length, iteration)).get)
    overall_perp.sort(key=dict(zip(overall_perp, iteration)).get)

    iteration.sort()

    # AVG plot
    axes2 = plt.gca()
    axes2.set_xlim([0, max(iteration)])
    axes2.set_ylim([0, max(avg_length)])
    axes2.set_xlabel('Iteration')
    axes2.set_ylabel('Average Bucket Length')
    axes2.set_title('Avg Bucket Length vs Iteration')
    #fig2 = plt.scatter(iteration, avg_length)
    fig2 = plt.plot(iteration, avg_length)
    axes.plot()
    filename = os.path.join(output_path, "average_bucket_length.png")
    plt.savefig(filename)
    plt.close()

    # MEAN plot
    axes3 = plt.gca()
    axes3.set_xlim([0, max(iteration)])
    axes3.set_ylim([0, max(avg_length)])
    axes3.set_xlabel('Iteration')
    axes3.set_ylabel('Mean Bucket Length')
    axes3.set_title('Mean Bucket Length vs Iteration')
    #fig2 = plt.scatter(iteration, mean_length)
    fig2 = plt.plot(iteration, mean_length)
    axes.plot()
    filename = os.path.join(output_path, "mean_bucket_length.png")
    plt.savefig(filename)
    plt.close()

    # PERP plot
    axes4 = plt.gca()
    axes4.set_xlim([0, max(iteration)])
    axes4.set_ylim([0, 30])
    axes4.set_xlabel('Iteration')
    axes4.set_ylabel('Perplexity')
    axes4.set_title('Perplexity vs Iteration')
    #fig2 = plt.scatter(iteration, overall_perp)
    fig2 = plt.plot(iteration, overall_perp)
    axes.plot()
    filename = os.path.join(output_path, "perplexity_iteration.png")
    plt.savefig(filename)
    plt.close()



