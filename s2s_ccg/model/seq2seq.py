import random

import numpy as np
import tensorflow as tf

from s2s_ccg.markers import Markers


class Seq2SeqSupertaggerModel(object):
    """Sequence-to-sequence model for CCG supertagging."""
    def __init__(self,
                 source_vocab_size,
                 target_vocab_size,
                 buckets,
                 batch_size,
                 unit_count,
                 layer_count,
                 learning_rate,
                 learning_rate_decay_factor=0.99,
                 max_gradient_norm=5.0,
                 cell_type='gru',
                 forward_only=False,
                 dtype=tf.float32):
        """Create the sequence-to-sequence model.

        Args:
            source_vocab_size (int): Size of the source vocabulary (words)
            target_vocab_size (int): Size of the target vocabulary (CCG tags)
            batch_size (int): Size of the batches used during training
            unit_count (int): Number if units in each layer.
            layer_count (int): Number of layers in the model.
            bucket_size (int): Size of one bucket.
            max_tokens (int): Maximum sentence length; sentences which are
                longer than this will be ignored.
            learning_rate (float): Learning rate to use.
            max_gradient_norm: Gradients will be clipped to maximally
                this norm.
            cell_type: Which type of RNN cell to use. Defaults to `gru`, which
                means we'll use the TensorFlow implementation of a GRU cell.
        """
        self.source_vocab_size = source_vocab_size
        self.target_vocab_size = target_vocab_size
        self.batch_size = batch_size
        self.unit_count = unit_count
        self.layer_count = layer_count
        self.learning_rate = tf.Variable(
            float(learning_rate),
            trainable=False,
            dtype=dtype
        )
        self.learning_rate_decay_op = self.learning_rate.assign(
            self.learning_rate * learning_rate_decay_factor
        )
        self.max_gradient_norm = max_gradient_norm
        self.cell_type = cell_type
        self.dtype = dtype

        self.global_step = tf.Variable(0, trainable=False)

        if layer_count > 1:
            self.cell = tf.contrib.rnn.MultiRNNCell(
                [
                    self._get_single_cell(unit_count, cell_type) for \
                        _ in range(layer_count)
                ]
            )
        else:
            self.cell = self._get_single_cell(unit_count, cell_type)

        # Feeds for inputs.
        self.encoder_inputs = []
        self.decoder_inputs = []
        self.target_weights = []

        for i in range(buckets[-1]):
          self.encoder_inputs.append(
            tf.placeholder(
                tf.int32,
                shape=[None],
                name="encoder{0}".format(i)
            )
        )

        for i in range(buckets[-1] + 1):
            self.decoder_inputs.append(
                tf.placeholder(
                    tf.int32,
                    shape=[None],
                    name="decoder{0}".format(i)
                )
            )
            self.target_weights.append(
                tf.placeholder(
                    dtype,
                    shape=[None],
                    name="weight{0}".format(i)
                )
            )

        # Our targets are decoder inputs shifted by one.
        targets = [
            self.decoder_inputs[i + 1] for i in range(
                len(self.decoder_inputs) - 1
            )
        ]

        if forward_only:
            self.outputs, self.losses = tf.contrib.legacy_seq2seq.model_with_buckets(
                self.encoder_inputs,
                self.decoder_inputs,
                targets,
                self.target_weights,
                tuple((i, i) for i in buckets),
                lambda x, y: self._seq2seq_func(x, y, True),
            )
        else:
            self.outputs, self.losses = tf.contrib.legacy_seq2seq.model_with_buckets(
              self.encoder_inputs,
              self.decoder_inputs,
              targets,
              self.target_weights,
              tuple((i, i) for i in buckets),
              lambda x, y: self._seq2seq_func(x, y, False),
            )

        params = tf.trainable_variables()

        if not forward_only:
            # Gradients and SGD updates for training the model
            self.gradient_norms = []
            self.updates = []

            opt = tf.train.GradientDescentOptimizer(self.learning_rate)

            for b in range(len(buckets)):
                gradients = tf.gradients(self.losses[b], params)
                clipped_gradients, norm = tf.clip_by_global_norm(
                    gradients,
                    self.max_gradient_norm,
                )
                self.gradient_norms.append(norm)
                self.updates.append(
                    opt.apply_gradients(
                        zip(clipped_gradients, params),
                        global_step=self.global_step,
                    )
                )

        self.saver = tf.train.Saver(tf.global_variables(), max_to_keep=0)


    def _get_single_cell(self, unit_count, cell_type):
        if cell_type == 'gru':
            return tf.contrib.rnn.GRUCell(unit_count)
        elif cell_type == 'lstm':
            return tf.contrib.rnn.BasicLSTMCell(unit_count)

    def _seq2seq_func(self, encoder_inputs, decoder_inputs, do_decode=False):
        return tf.contrib.legacy_seq2seq.embedding_attention_seq2seq(
            encoder_inputs,
            decoder_inputs,
            self.cell,
            num_encoder_symbols=self.source_vocab_size,
            num_decoder_symbols=self.target_vocab_size,
            embedding_size=self.unit_count,
            # TODO: Do we need this?
            #output_projection=output_projection,
            feed_previous=do_decode,
            dtype=self.dtype
        )

    def step(self, session, encoder_inputs, decoder_inputs, target_weights, bucket, bucket_id, forward_only=False):
        if len(encoder_inputs) != bucket:
            raise ValueError('Encoder length must match the bucket length ({} != {})'.format(
                len(decoder_inputs),
                bucket,
            ))

        if len(decoder_inputs) != bucket:
            raise ValueError('Decoder length must match the bucket length ({} != {})'.format(
                len(decoder_inputs),
                bucket,
            ))

        if len(target_weights) != bucket:
            raise ValueError('Weight length must match the bucket length ({} != {})'.format(
                len(decoder_inputs),
                bucket,
            ))

        encoder_size, decoder_size = bucket, bucket

        # Input feed
        input_feed = {}

        for idx in range(encoder_size):
            input_feed[self.encoder_inputs[idx].name] = encoder_inputs[idx]

        for idx in range(decoder_size):
            input_feed[self.decoder_inputs[idx].name] = decoder_inputs[idx]
            input_feed[self.target_weights[idx].name] = target_weights[idx]

        last_target = self.decoder_inputs[decoder_size].name
        input_feed[last_target] = np.zeros([self.batch_size], dtype=np.int32)

        # Output feed
        if forward_only:
            output_feed = [self.losses[bucket_id]]

            for idx in range(decoder_size):
                output_feed.append(self.outputs[bucket_id][idx])
        else:
            output_feed = [
                self.updates[bucket_id],           # Update Op that does SGD
                self.gradient_norms[bucket_id],    # Gradient norm
                self.losses[bucket_id]             # Loss for this batch
            ]

        outputs = session.run(output_feed, input_feed)

        if forward_only:
            # No gradient norm, loss, outputs
            return None, outputs[0], outputs[1:]
        else:
            # Gradient norm, loss, no outputs
            return outputs[1], outputs[2], None

    def get_batch(self, data, bucket, decode=False):
        encoder_size, decoder_size = bucket, bucket

        encoder_inputs = []
        decoder_inputs = []

        for _ in range(self.batch_size):
            sentence = random.choice(data.bucketized_sentences[bucket])
            encoder_input, decoder_input  = data.tensorize(sentence, bucket)

            # Pad encoder input to bucket length
            encoder_pad_length = bucket - len(encoder_input)

            if encoder_pad_length < 0:
                raise ValueError(
                    'Encoder input is longer than bucket: {} > {}'.format(
                        len(encoder_input),
                        bucket,
                    )
                )

            encoder_input = list(encoder_input) + [Markers.PAD_ID] * encoder_pad_length

            # Encoder inputs are reversed
            encoder_inputs.append(encoder_input[::-1])

            # Decoder input get an extra start symbol and are padded then
            decoder_pad_length = bucket - len(decoder_input) - 1

            if not decode:
                decoder_input = [Markers.START_ID] + list(decoder_input) + [Markers.PAD_ID] * decoder_pad_length
            else:
                # When decoding, we do not want the encoder to know the actual result
                decoder_input = [Markers.START_ID] + [Markers.UNKNOWN_ID for _ in range(len(decoder_input - 1))] \
                                + [Markers.END_ID] + [Markers.PAD_ID] * decoder_pad_length

            decoder_inputs.append(decoder_input)

        # Create batch-major vectors from the data selected above
        batch_encoder_inputs = []
        batch_decoder_inputs = []
        batch_weights = []

        # Batch encoder inputs are re-indexed encoder_inputs
        for enc_idx in range(encoder_size):
            batch_encoder_inputs.append(
                np.array(
                    [encoder_inputs[batch_idx][enc_idx] for batch_idx in range(self.batch_size)],
                    dtype=np.int32
                )
            )

        # Batch decoder inputs are re-indexed decoder_inputs
        for dec_idx in range(decoder_size):
            batch_decoder_inputs.append(
                np.array(
                    [decoder_inputs[batch_idx][dec_idx] for batch_idx in range(self.batch_size)],
                    dtype=np.int32
                )
            )

            # Create target weights
            batch_weight = np.ones(self.batch_size, dtype=np.float32)

            for batch_idx in range(self.batch_size):
                # The corresponding target is decoder_input shifted by 1 forward
                if dec_idx < decoder_size - 1:
                    target = decoder_inputs[batch_idx][dec_idx + 1]

                # Set weight to 0 if the corresponding target is a PAD or END symbol
                if dec_idx == decoder_size - 1 or target in [Markers.PAD_ID, Markers.END_ID]:
                    batch_weight[batch_idx] = 0.0

            batch_weights.append(batch_weight)

        return (
            batch_encoder_inputs,
            batch_decoder_inputs,
            batch_weights,
        )
