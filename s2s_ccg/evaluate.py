import logging

import numpy as np
import tensorflow as tf

from s2s_ccg.data import Data
from s2s_ccg.utils import tempinput


def evaluate_checkpoint(model, session, checkpoint_path, data, supertag_space, embedding_space):
    if not tf.train.checkpoint_exists(checkpoint_path):
        logging.error('Checkpoint at {} does not exist, aborting.'.format(checkpoint_path))

    logging.info('Restoring model parameters from checkpoint at \'{}\''.format(checkpoint_path))
    model.saver.restore(session, checkpoint_path)

    correct_sentences = 0
    correct_tags = 0
    total_tags = 0
    bucket_stats = {}
    stats = {}

    for bucket in data.buckets:
        bucket_id = data.bucket_id(bucket)
        sentences = data.bucketized_sentences[bucket]

        correct_sentences_in_bucket = 0
        correct_tags_in_bucket = 0
        bucket_size = len(sentences)
        bucket_tag_size = 0

        if not bucket_size:
            logging.info('Skipping empty bucket {}'.format(bucket))
            continue

        logging.info('Evaluating {} sentences in bucket {}'.format(bucket_size, bucket))

        for words, supertags, _ in sentences:
            logging.info('Predicting sentence \'{}\''.format(' '.join(words)))
            logging.info('Supertags: {}'.format(' '.join(supertags)))

            stagged_sentence = ' '.join(
                '|'.join(word_and_tag) for word_and_tag in zip(words, supertags) if word_and_tag[0] and word_and_tag[1]
            )

            with tempinput(stagged_sentence) as tempfile:
                one_sentence_data = Data(tempfile, supertag_space, embedding_space)

            # Get a 1-element batch to feed the sentence to the model.
            encoder_inputs, decoder_inputs, target_weights = model.get_batch(
                one_sentence_data,
                bucket,
                decode=True,
            )

            # Get output logits for the sentence.
            _, loss, output_logits = model.step(
                session,
                encoder_inputs,
                decoder_inputs,
                target_weights,
                bucket,
                bucket_id,
                True
            )

            outputs = [np.argmax(logit) for logit in output_logits]
            output_tags = [supertag_space.feature(idx) for idx in outputs]

            logging.info('Outputs: {}'.format(outputs))
            logging.info('Output tags: {}'.format(output_tags))

            # Count correct tags
            tag_count = len(supertags)
            bucket_tag_size += tag_count
            correct_tags_in_sentence = 0

            for idx, stag in enumerate(supertags):
                if stag == output_tags[idx]:
                    correct_tags_in_sentence += 1

            logging.info('Tagged {} words correctly'.format(correct_tags_in_sentence))

            if tag_count == correct_tags_in_sentence:
                logging.info('Tagged complete sentence correctly')
                correct_sentences_in_bucket += 1

            correct_tags_in_bucket += correct_tags_in_sentence

        logging.info('Predicted {} tags correctly in bucket {}'.format(correct_tags_in_bucket, bucket))
        logging.info('Predicted {} sentences correctly in bucket {}'.format(correct_sentences_in_bucket, bucket))

        bucket_stats[bucket] = {}
        bucket_stats[bucket]['total_tags'] = bucket_tag_size
        bucket_stats[bucket]['correct_tags'] = correct_tags_in_bucket
        bucket_stats[bucket]['total_sentences'] = bucket_size
        bucket_stats[bucket]['correct_sentences'] = correct_sentences_in_bucket
        bucket_stats[bucket]['sentence_accuracy'] = correct_sentences_in_bucket / bucket_size
        bucket_stats[bucket]['tag_accuracy'] = correct_tags_in_bucket / bucket_tag_size

        correct_tags += correct_tags_in_bucket
        correct_sentences += correct_sentences_in_bucket
        total_tags += bucket_tag_size

    stats['bucket_stats'] = bucket_stats
    stats['total_sentences'] = data.size
    stats['correct_sentences'] = correct_sentences
    stats['total_tags'] = total_tags
    stats['correct_tags'] = correct_tags
    stats['sentence_accuracy'] = correct_sentences / data.size
    stats['tag_accuracy'] = correct_tags / total_tags

    return stats
