"""Module for reading `stagged` files.

`stagged` files are text files in a special format as used by Lewis et al.
(2016). They contain one sentence per line, which is already split into
words, where punctuation is treated as words as well. Words are annotated
with their corresponding POS tag and CCG supertag from CCGBank, separated by
a pipe (`|`). It is also possible to omit the POS tag, which means the word
in question will be annotated with the CCG supertag only.
This module provides a class `StaggedReader`, which is largely based on Lewis'
original code [1], but adapted for Python 3 and TensorFlow 1.0 as well as slightly
rewritten.

[1] TODO: Link to taggerflow/ccgbank.py
"""
class StaggedReader():
    """Class for reading `stagged` files.

    Args:
        filename (str): Path to the stagged file to read.

    Attributes:
        raw_sentences (list): The list of sentences read from the stagged file.
        aligned_sentences (list): A list of sentences aligned with
            `align_sentences`, i.e. a list of 3-tuples containing
            (i) a list of the words in the sentence
            (ii) a list of the corresponding CCG supertags, in order; and
            (iii) whether the sentence is part of the tri-training data.
        ccg_tags (list): Contains only the ccg tags. Used primarily for eval

    """
    def __init__(self, filename):
        self.filename = filename
        self.raw_sentences = []
        self.aligned_sentences = []
        self.ccg_tags = []

    def get_word_and_supertag(self, split):
        """Get the word and supertag from a given split.

        Args:
            split (list): A list of length 2 or 3, either containing (in this
                order) the word and its supertag or the word, its POS tag,
                and its supertag respectively. Other split lengths are not
                supported.

        Raises:
            ValueError: When the given split is of an unsupported length.
        """
        if len(split) == 3:
            return (split[0].strip(), split[2].strip())
        elif len(split) == 2:
            return (split[0].strip(), split[1].strip())
        else:
            raise ValueError('Unknown split length: {}'.format(split))

    def get_sentences(self):
        """Read the raw sentences from the file."""
        with open(self.filename, 'r') as f:
            self.raw_sentences = [l for l in (line.strip() for line in f) if l]

    def align_sentences(self, is_tritrain=False):
        """Align words in each sentence with their supertags."""
        words_and_supertags = (
            zip(
                *[self.get_word_and_supertag(annotated_word.split('|')) \
                    for annotated_word in raw_sentence.split(' ')]
            ) for raw_sentence in self.raw_sentences
        )

        self.aligned_sentences = [
            (
                tuple(words),
                tuple(supertags),
                is_tritrain,
            ) for words, supertags in words_and_supertags
        ]

    def evaluate_sentences(self):
        """ Will return only the ccg tags of the sentences."""

        # call this autmatically
        self.get_sentences()
        # preallocate
        self.ccg_tags = [0] * len(self.raw_sentences)

        for i in range(len(self.raw_sentences)):

            self.ccg_tags[i] = self.raw_sentences[i].split(" ")
        # also include selection for ccg tag only
            temp = []
            for j in range(len(self.ccg_tags[i])):
                # assert correct length, similar to get_word_and_supertag
                token = self.ccg_tags[i][j].split("|")
                if (len(token) == 3):
                    temp.append(token[2].strip())
                else:
                    temp.append(token[1].strip())


            self.ccg_tags[i] = temp

        return self.ccg_tags
