#!/usr/bin/env python
from contextlib import contextmanager
import logging
import os

import click

from s2s_ccg.cli import analyze, ccgbank, evaluate, train


@contextmanager
def log_to_file(directory, filename):
    handler = logging.FileHandler(os.path.join(directory, filename))
    logging.getLogger().addHandler(handler)
    yield
    logging.getLogger().removeHandler(handler)


def init_logging():
    """Initialize logging."""
    stream_handler = logging.StreamHandler()
    logging.getLogger().addHandler(stream_handler)
    logging.getLogger().setLevel(logging.INFO)


@click.group()
def cli():
    init_logging()


cli.add_command(analyze)
cli.add_command(ccgbank)
cli.add_command(evaluate)
cli.add_command(train)


if __name__=='__main__':
    cli()
