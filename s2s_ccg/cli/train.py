import datetime
import logging
import os
import random
import time

import click
import tensorflow as tf

from s2s_ccg.data import read_data
from s2s_ccg.defaults import (
    DEFAULT_BATCH_SIZE,
    DEFAULT_BUCKET_SIZE,
    DEFAULT_LAYERS,
    DEFAULT_LEARNING_RATE,
    DEFAULT_LEARNING_RATE_DECAY_FACTOR,
    DEFAULT_MAX_GRADIENT_NORM,
    DEFAULT_MAX_TOKENS,
    DEFAULT_STEPS,
    DEFAULT_UNITS,
    RNN_CELL_CHOICES,
)
from s2s_ccg.model import create_model, init_spaces
from s2s_ccg.train import (
    bucket_distribution,
    get_random_bucket,
    maybe_evaluate,
)


@click.command(help='Train the seq2seq supertagger model')
@click.option('--categories', type=click.Path(), help='Path to the categories file')
@click.option('--embeddings', type=click.Path(), help='Path to the embeddings file')
@click.option('--dev-data', type=click.Path(), help='Path to the development data')
@click.option('--training-data', type=click.Path(), help='Path to the training data')
@click.option('--tritrain-data', type=click.Path(), help='Path to the tritrain data')
@click.option('--output-path', '-o', type=click.Path(), default='output', help='Output directory')
@click.option('--experiment', '-e', type=click.STRING, default='seq2seq', help='Experiment name (use to distinguish between different runs)')
@click.option('--batch-size', type=click.INT, default=DEFAULT_BATCH_SIZE, help='Size of one batch used for training')
@click.option('--layers', type=click.INT, default=DEFAULT_LAYERS, help='Number of layers in the model')
@click.option('--units', type=click.INT, default=DEFAULT_UNITS, help='Number of hidden units')
@click.option('--learning-rate', type=click.FLOAT, default=DEFAULT_LEARNING_RATE, help='Learning rate to use for training')
@click.option('--learning-rate-decay-factor', type=click.FLOAT, default=DEFAULT_LEARNING_RATE_DECAY_FACTOR, help='How much to decrease learning rate')
@click.option('--max-gradient-norm', type=click.FLOAT, default=DEFAULT_MAX_GRADIENT_NORM, help='Maximum gradient norm after which gradients will be clipped')
@click.option('--bucket-size', type=click.INT, default=DEFAULT_BUCKET_SIZE, help='Size of the buckets for bucketized training')
@click.option('--max-tokens', type=click.INT, default=DEFAULT_MAX_TOKENS, help='Maximum number of tokens in a sentence (larger ones will be ignored)')
@click.option('--cell-type', type=click.Choice(RNN_CELL_CHOICES), default='gru', help='RNN cell type to use')
@click.option('--steps-per-checkpoint', type=click.INT, default=DEFAULT_STEPS, help='How many training steps to do per checkpoint')
def train(categories,
          embeddings,
          dev_data,
          training_data,
          tritrain_data,
          output_path,
          experiment,
          batch_size,
          layers,
          units,
          learning_rate,
          learning_rate_decay_factor,
          max_gradient_norm,
          bucket_size,
          max_tokens,
          cell_type,
          steps_per_checkpoint):
    checkpoint_path = os.path.join(output_path, experiment)

    if not os.path.exists(checkpoint_path):
        logging.info('Creating output path at {}'.format(checkpoint_path))
        os.makedirs(checkpoint_path)

    # Log to file in output path
    logger = logging.getLogger()
    logger.addHandler(
        logging.FileHandler(os.path.join(checkpoint_path, 'log'))
    )

    # Initialize spaces & read data
    supertag_space, turian_space = init_spaces(categories, embeddings)
    dev = read_data(supertag_space, turian_space, dev_data, bucket_size, max_tokens)
    training = read_data(supertag_space, turian_space, training_data, bucket_size, max_tokens)

    # Use tritrain data if available
    if tritrain_data:
        tritrain = read_data(supertag_space, turian_space, tritrain_data, bucket_size, max_tokens)
        train_data = training + tritrain
    else:
        train_data = training


    # Get training data distribution over buckets
    train_dist = bucket_distribution(train_data)

    with tf.Session() as session:
        # Create the model, read parameters from checkpoint if it exists
        logging.info('Creating model')
        model = create_model(
            session,
            turian_space.size(),
            supertag_space.size(),
            training.buckets,
            batch_size,
            units,
            layers,
            learning_rate,
            learning_rate_decay_factor,
            max_gradient_norm,
            cell_type,
        )

        ckpt = tf.train.get_checkpoint_state(checkpoint_path)

        if ckpt and tf.train.checkpoint_exists(ckpt.model_checkpoint_path):
            logging.info('Reading model parameters from \'{}\''.format(checkpoint_path))
            model.saver.restore(session, ckpt.model_checkpoint_path)
        else:
            logging.info('Initialized new model -- initializing session')
            session.run(tf.global_variables_initializer())

        # Training loop
        logging.info('Starting training loop')

        train_on = True
        step_time = 0.0
        loss = 0.0
        current_step = 0
        previous_losses = []

        while train_on:
            # Note start time for regular evaluation
            start_time = time.time()
            logging.info('Step {} at {}'.format(
                current_step,
                datetime.datetime.fromtimestamp(start_time),
            ))

            # Get a random bucket according to data distribution
            #bucket = get_random_bucket(training.buckets, train_dist)
            bucket = random.choice(train_data.buckets)

            # Skip empty buckets
            if not train_data.bucketized_sentences[bucket]:
                logging.info('Skipping empty bucket {}'.format(bucket))
                continue

            logging.info('Using bucket {}'.format(bucket))

            # Get a batch of sentences from the bucket
            encoder_inputs, decoder_inputs, weights = model.get_batch(train_data, bucket)

            # Do the actual training step
            gradient_norm, step_loss, outputs = model.step(
                session,
                encoder_inputs,
                decoder_inputs,
                weights,
                bucket,
                training.bucket_id(bucket),
            )

            # Measure time needed for step
            step_time += (time.time() - start_time) / steps_per_checkpoint

            # Update loss
            loss += step_loss / steps_per_checkpoint

            current_step += 1

            train_on, evaluated = maybe_evaluate(
                model,
                session,
                dev,
                current_step,
                steps_per_checkpoint,
                step_time,
                loss,
                previous_losses,
                output_path,
                experiment
            )

            if evaluated:
                # Reset timer and loss
                step_time = 0.0
                loss = 0.0
