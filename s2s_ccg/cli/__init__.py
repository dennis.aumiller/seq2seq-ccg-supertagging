"""CLI module for s2s-ccg-supertagging"""
from .analyze import analyze
from .ccgbank import ccgbank
from .evaluate import evaluate
from .train import train
